provider "aws"{
 region = "ap-south-1"
}

resource "aws_instance" "example" {
  ami = "ami-0851b76e8b1bce90b"
  instance_type = "t2.micro"
  key_name = "id_rsa"

  provisioner "remote-exec"{
    inline = [
      "touch hello.txt",
      "echo helloworld remote provisioner >> hello.txt",
    ]
  }

  connection{
    type = "ssh"
    host = self.public_ip
    user = "ubuntu"
    private_key = file("/home/ubuntu/.ssh/id_rsa")
    timeout     = "4m"
  }
}

resource "aws_key_pair" "deployer" {
  key_name = "id_rsa"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCkCHVAqU+72YamCnXgb3MIyjDtKFwCP2jeg16chAaGScuCjbov/DF8mbxp4MzN2/S76gyVjKcFEZ4bDimyQktEQglhSTjUpIYHScVdwVgK46XEEsSvJJ+kMf5nQ9aSkj3XD+cKqNpIvpfCu7BSPEeHaTXj5t6HR74I4d/x+pj0PX25kNZy7VCz61Ec+4d8KBEupsg1KW6WYdvgn3nnw8NIhSsi3/ECZO04iLDvUrxOrW6cAeO4eeyiu4FeE2a8+YOxq+Z8ouo5P/GTgx4/aXfYgdUHR1ebOpZLNYEC0pVuOnSfcIh6V51bZ6EraDuGsmKArEkJm+Fzq/vi/A61PU/5zfgOkIRmQB29lEPUJap+ow3Jtt54zFhWH/A3AIDPIvFOwLNsIE76X2EEDV+ZcfYNQ3Ypj6kZ74jJ3BXPWLS1fcbSJxBomXR5iDj/iID6QoRD52IfDHVwfZo2szidhdcIP+v8dWJPTGynMJ+77iZV6tfYJFwdfhPciYn8zPMBOPM= ubuntu@ip-172-31-6-167"
}
